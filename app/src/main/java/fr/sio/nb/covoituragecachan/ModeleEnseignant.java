package fr.sio.nb.covoituragecachan;

import java.util.ArrayList;
import java.util.List;

public class ModeleEnseignant {


    private static ModeleEnseignant modele = null;
    private List<Enseignant> lesEnseignants = new ArrayList<Enseignant>();

    private ModeleEnseignant (){       // C'est le singleton

        super();
        this.peupler();
    }

    public static ModeleEnseignant getModele (){

        if  (modele == null){
            modele = new ModeleEnseignant();

        }
        return modele;
    }

    public boolean verificationLoginEnseignant(Enseignant inscrit){
        boolean res = false;
        for(Enseignant unElement : this.lesEnseignants){
            if(inscrit.getLogin().equals(unElement.getLogin())){
                res = true;
            }
        }
        return res;
    }

    public void addEnseignant(Enseignant enseignant){

        lesEnseignants.add(enseignant);

    }
    public List<Enseignant> getLesEnseignants(){

        return this.lesEnseignants;
    }

    public void peupler(){

        lesEnseignants.add(new Enseignant("Koba","Lad","KBL","123","adresse@hotmail.fr"));
        lesEnseignants.add(new Enseignant("Jul","Ov","jl","456i","adresse@gmail.com"));





    }
}
