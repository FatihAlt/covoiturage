package fr.sio.nb.covoituragecachan;

public class Utilisateur {

    protected String nom;
    protected String prenom;
    protected String login;
    protected String mdp;

    public Utilisateur(String nom,String prenom,String login,String mdp){
        this.nom=nom;
        this.prenom = prenom;
        this.login = login;
        this.mdp = mdp;
    }

    public Utilisateur(String nom,String prenom){
        this.prenom = prenom;
        this.nom = nom;
    }
    public String getNom() {

      return this.nom;
    }

    public void setNom(String nom) {

      this.nom = nom;
    }

    public String getPrenom() {

      return this.prenom;
    }

    public void setPrenom(String prenom) {

      this.prenom = prenom;
    }

    public String getLogin(){
      return this.login;
    }

    public void setLogin(String login){
      this.login = login;
    }

    public String getMdp(){
      return this.mdp;
    }
    public void setMdp(){
      this.mdp = mdp;
    }


    @Override
    public String toString() {
        return
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\''
                ;
    }

}
