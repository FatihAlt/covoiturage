package fr.sio.nb.covoituragecachan;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class ThirdActivity extends AppCompatActivity {
TextView idNom;
TextView idPrenom;
TextView idLogin;
TextView idMdp;
TextView idMail;
Button btnRetour;
    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        idNom = (TextView) findViewById(R.id.tvNom);
        idPrenom = (TextView) findViewById(R.id.tvPrenom);
        idLogin = (TextView) findViewById(R.id.tvLogin);
        idMdp = (TextView) findViewById(R.id.tvMdp);
        idMail = (TextView) findViewById(R.id.tvMail);
        btnRetour = (Button) findViewById(R.id.btnRetour);
        Bundle paquet = this.getIntent().getExtras();


        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonEnseignant = paquet.getString("enseignantSaisi");
        Log.i("ThirdActivity::jsonEnseignant",jsonEnseignant);
        Enseignant unProf = gson.fromJson(jsonEnseignant, Enseignant.class);
        Log.i("ThirdActivity::fromJson",unProf.toString());

        String nomSaisi = unProf.getNom();
        String prenomSaisi = unProf.getPrenom();
        String loginSaisi = unProf.getLogin();
        String mdpSaisi = unProf.getMdp();
        String mailSaisi = unProf.getMail();

        idNom.setText("Nom : "+nomSaisi);
        idPrenom.setText("Prenom : "+prenomSaisi);
        idLogin.setText("Login : "+loginSaisi);
        idMdp.setText("Votre mot de passe : "+mdpSaisi);
        idMail.setText("Mail: "+mailSaisi);
        boolean existe = ModeleEnseignant.getModele().verificationLoginEnseignant(unProf);
        idMail.setText("Verif "+existe);
    }

    public void retourner(View vue){
        Intent intentRenvoyer  = new Intent(this,MainActivity.class);
        startActivity(intentRenvoyer);
    }
}
