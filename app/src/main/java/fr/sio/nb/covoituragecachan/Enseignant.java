package fr.sio.nb.covoituragecachan;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.EditText;

import java.io.Serializable;

public class Enseignant extends Utilisateur implements Serializable
{
    private String mail;

    public Enseignant (String nom, String prenom,String login, String mdp, String mail)
    {

        super(nom,prenom,login,mdp);
        this.mail = mail;
    }


    public String getMail()
    {
        return mail;
    }

    public void setMail(String mail)
    {
        this.mail = mail;
    }

    @Override
    public String toString()
    {
        return super.toString() + "mail" + this.mail;

    }
}
