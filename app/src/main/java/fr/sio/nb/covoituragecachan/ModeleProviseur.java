package fr.sio.nb.covoituragecachan;

import java.util.ArrayList;
import java.util.List;

public class ModeleProviseur {

    private static ModeleProviseur modele = null;
    private List<Proviseur> lesProviseurs = new ArrayList<Proviseur>();

    private ModeleProviseur ()
    {

        super();
        this.peupler();
    }

    public static ModeleProviseur getModele ()
    {

        if  (modele == null){
            modele = new ModeleProviseur();
        }
        return modele;
    }

    public List<Proviseur> getLesProviseurs()
    {

        return this.lesProviseurs;
    }

    public void peupler()
    {

        lesProviseurs.add(new Proviseur("Hakan","Balta","proviseur1","test123"));
        lesProviseurs.add(new Proviseur("Sami","Sad","proviseur2","test456"));

    }
}
