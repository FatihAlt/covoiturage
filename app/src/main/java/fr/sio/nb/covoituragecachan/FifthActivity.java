package fr.sio.nb.covoituragecachan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FifthActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    TextView nomProviseur;
    TextView prenomProviseur;
    Button btnDeconnexion;
    ListView lvEnseignant;
    Proviseur leProviseur;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth);

        nomProviseur = (TextView)findViewById(R.id.nomProviseur);
        prenomProviseur = (TextView)findViewById(R.id.prenomProviseur);
        btnDeconnexion = (Button)findViewById(R.id.btnDeconnecter);
        lvEnseignant = (ListView)findViewById(R.id.idLvEnseignant);

        Bundle paquet = this.getIntent().getExtras();


        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonProviseur = paquet.getString("proviseurSaisi");

        leProviseur = gson.fromJson(jsonProviseur, Proviseur.class);
        Log.i("ThirdActivity::fromJson",leProviseur.toString());

        String nom = leProviseur.getNom();
        String prenom = leProviseur.getPrenom();
        nomProviseur.setText(nom+" ");
        prenomProviseur.setText(prenom);

        ItemEnseignantAdaptater adapter = new ItemEnseignantAdaptater(this,
                ModeleEnseignant.getModele().getLesEnseignants());
        lvEnseignant.setAdapter(adapter);
        lvEnseignant.setOnItemClickListener(this);
    }
    public void deconnexion(View vue){
        Intent intentRenvoyer  = new Intent(this,MainActivity.class);
        startActivity(intentRenvoyer);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Enseignant profSelectionne = ModeleEnseignant.getModele().getLesEnseignants()
                .get(position);

        Gson gson = new GsonBuilder().create();
        String jsonProf = gson.toJson(profSelectionne);
        String jsonProviseur = gson.toJson(leProviseur);
        Bundle paquet = new Bundle();
        paquet.putString("profChoisi",jsonProf);
        paquet.putString("proviseurSaisi",jsonProviseur);
        Intent intentEnvoyer = new Intent(this,SixthActivity.class);
        intentEnvoyer.putExtras(paquet);
        startActivity(intentEnvoyer);
    }
}
