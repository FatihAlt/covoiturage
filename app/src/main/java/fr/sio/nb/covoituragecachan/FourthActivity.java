package fr.sio.nb.covoituragecachan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FourthActivity extends AppCompatActivity {
    Button btnConnecter;
    EditText txtLogin;
    EditText txtMdp;
    RadioButton btnProf;
    RadioButton btnProviseur;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);

        txtLogin = (EditText)findViewById(R.id.login);
        txtMdp = (EditText)findViewById(R.id.mdp);

        btnProf = (RadioButton)findViewById(R.id.professeur);
        btnProviseur = (RadioButton)findViewById(R.id.proviseur);

        btnConnecter = (Button)findViewById(R.id.btnConnect);
    }

    public void btnConnect(View vue){
        String login = txtLogin.getText().toString();
        String mdp = txtMdp.getText().toString();
        String nom;
        if(btnProf.isChecked()){
            Enseignant leProf = null;
            for(Enseignant unEnseignant : ModeleEnseignant.getModele().getLesEnseignants()){
                if(unEnseignant.getMdp().equals(mdp) && unEnseignant.getLogin().equals(login)){
                    leProf = unEnseignant;
                }

            }
            if (leProf != null) {
                Toast.makeText(this,leProf.getNom(), Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(this,"identifiants incorrect", Toast.LENGTH_LONG).show();
            }

        }else if (btnProviseur.isChecked())
        {
            Proviseur leProviseur = null;
            for(Proviseur unProviseur : ModeleProviseur.getModele().getLesProviseurs()){
                    if(unProviseur.getMdp().equals(mdp) && unProviseur.getLogin().equals(login)){
                        leProviseur = unProviseur;
                    }
            }
            if(leProviseur != null){

                Gson gson = new GsonBuilder().create();
                String jsonProviseur = gson.toJson(leProviseur);
                Bundle paquet = new Bundle();
                paquet.putString("proviseurSaisi",jsonProviseur);
                Intent intentEnvoyer = new Intent(this,FifthActivity.class);
                intentEnvoyer.putExtras(paquet);
                startActivity(intentEnvoyer);
                Toast.makeText(this,leProviseur.getNom(), Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,"Mauvais identifiants", Toast.LENGTH_LONG).show();
            }

        }
        else{
            Toast.makeText(this,"Professeur ou Proviseur ?", Toast.LENGTH_LONG).show();
        }
    }
}
