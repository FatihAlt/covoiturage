package fr.sio.nb.covoituragecachan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnConnexion;
    Button btnInscription;
    Button btnPropos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnConnexion = (Button) findViewById(R.id.btnConnexion);
        btnInscription = (Button) findViewById(R.id.btnInscription);
        btnPropos = (Button) findViewById(R.id.btnPropos);
    }

public void btnPropos(View vue){
    Toast.makeText(this,"A propos", Toast.LENGTH_LONG).show();
}

    public void btnConnexion(View vue){
        Toast.makeText(this,"Connexion", Toast.LENGTH_LONG).show();
        Intent intentSeDiriger = new Intent(this, FourthActivity.class);

        startActivity(intentSeDiriger);
    }

    public void btnInscription(View vue){
        Toast.makeText(this,"Inscription", Toast.LENGTH_LONG).show();
        Intent intentSeDiriger = new Intent(this, SecondActivity.class);

        startActivity(intentSeDiriger);
    }

}
