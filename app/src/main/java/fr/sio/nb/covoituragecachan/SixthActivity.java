package fr.sio.nb.covoituragecachan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class SixthActivity extends AppCompatActivity {
    TextView nomProf;
    TextView prenomProf;
    TextView mailProf;
    Proviseur leProviseur;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sixth);

        Bundle paquet = this.getIntent().getExtras();

        nomProf = (TextView)findViewById(R.id.tvNomChoisi);
        prenomProf = (TextView)findViewById(R.id.tvPrenomChoisi);
        mailProf = (TextView)findViewById(R.id.tvMailChoisi);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonProf = paquet.getString("profChoisi");

        Enseignant leProf = gson.fromJson(jsonProf, Enseignant.class);

        String jsonProviseur = paquet.getString("proviseurSaisi");

        leProviseur = gson.fromJson(jsonProviseur, Proviseur.class);
        Log.i("SixthActivity::fromJson",leProf.toString());

        String nom = leProf.getNom();
        String prenom = leProf.getPrenom();
        String mail = leProf.getMail();
        nomProf.setText(nom+" ");
        prenomProf.setText(prenom+" ");
        mailProf.setText(mail);
    }

    public void retour  (View vue){
        Gson gson = new GsonBuilder().create();
        String jsonProviseur = gson.toJson(leProviseur);
        Bundle paquet = new Bundle();
        paquet.putString("proviseurSaisi",jsonProviseur);
        Intent intentEnvoyer = new Intent(this,FifthActivity.class);
        intentEnvoyer.putExtras(paquet);
        startActivity(intentEnvoyer);
    }
}
