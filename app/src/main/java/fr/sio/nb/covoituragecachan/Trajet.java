package fr.sio.nb.covoituragecachan;

public class Trajet {

   private String ville;
   private int distance;

    public Trajet (String ville, int distance){
        this.ville = ville;
        this.distance = distance;
    }

    public String getVille (){

      return this.ville;
    }

    public int getDistance (){

      return this.distance;
    }

    public void setVille(String ville){

      this.ville = ville;
    }

    public void setDistance(int distance){

      this.distance  = distance;
    }
}
