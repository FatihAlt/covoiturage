package fr.sio.nb.covoituragecachan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SecondActivity extends AppCompatActivity {
EditText prenom;
EditText nom;
EditText mail;
Button btnValider;
EditText login;
EditText mdp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        nom = (EditText) findViewById(R.id.nom);
        prenom = (EditText) findViewById(R.id.prenom);
        login = (EditText) findViewById(R.id.login);
        mdp = (EditText) findViewById(R.id.mdp);
        mail = (EditText) findViewById(R.id.mail);
        btnValider = (Button) findViewById(R.id.btnValider);
    }



    public void envoyer (View vue){

        String valeurNom = nom.getText().toString();
        String valeurPrenom = prenom.getText().toString();
        String valeurLogin = login.getText().toString();
        String valeurMdp = mdp.getText().toString();
        String valeurMail = mail.getText().toString();

        Enseignant enseignant = new Enseignant(valeurNom,valeurPrenom,valeurLogin,valeurMdp,valeurMail);
        Log.i("leProf",enseignant.toString());

        Gson gson = new GsonBuilder().create();
        String jsonEnseignant = gson.toJson(enseignant);

      Bundle paquet = new Bundle();
        paquet.putString("enseignantSaisi",jsonEnseignant);

        Intent intentEnvoyer = new Intent(this,ThirdActivity.class);
        String emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";

        Pattern pattern = Pattern.compile(emailRegEx);
        Matcher matcher = pattern.matcher(mail.getText().toString());

        if (mail.getText().toString().isEmpty() || valeurNom.length() == 0 || valeurPrenom.length() == 0 || valeurLogin.length() == 0 || valeurMdp.length() == 0 ) {
            Toast.makeText(this, "Veuillez saisir toutes les informations ", Toast.LENGTH_LONG).show();
        } else if (!matcher.find()) {
            Toast.makeText(this, "Ceci n'est pas un email", Toast.LENGTH_LONG).show();
        } else {
            boolean existe =  ModeleEnseignant.getModele().verificationLoginEnseignant(enseignant);
            if(existe){
                Toast.makeText(this, "Ce login existe déjà ", Toast.LENGTH_LONG).show();
            }else{
                ModeleEnseignant.getModele().addEnseignant(enseignant);
                intentEnvoyer.putExtras(paquet);
                startActivity(intentEnvoyer);
            }

        }
}

}
