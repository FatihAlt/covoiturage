package fr.sio.nb.covoituragecachan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ItemEnseignantAdaptater extends ArrayAdapter<Enseignant> {
    public ItemEnseignantAdaptater (Context context, List<Enseignant> lesEnseignants) {
    super(context, -1, lesEnseignants );
}

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vItem = null;


        if (convertView != null) {
            vItem = convertView ;
        } else {
            vItem = layoutInflater.inflate(R.layout.item_enseignant, parent, false);
        }
        TextView tvNom = (TextView) vItem.findViewById(R.id.idTvNom);
        tvNom.setText(ModeleEnseignant.getModele().getLesEnseignants().
                get(position).getNom());
        TextView tvPrenom = (TextView) vItem.findViewById(R.id.idTvPrenom);
        tvPrenom.setText(""+ ModeleEnseignant.getModele().getLesEnseignants().
                get(position).getPrenom());
        return vItem;
    }
}
